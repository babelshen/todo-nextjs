export interface ITask {
    id: string;
    createdAt: Date;
    updatedAt: Date;
    name: string;
    description: string;
    priority: number;
    status: string;
}