import FormEditTask from "@/components/FormEditTask";
import CustomLink from "@/components/UI/CustomLink";
import { getTaskById } from "@/services/getTasks";
import { ITask } from "./interface";

type Props = {
    params: {
        id: string
    }
}

export const revalidate = 10;

const EditTaskPage = async ({params: {id}}: Props) => {
    const task: ITask = await getTaskById(id);
    return(
        <>
            <CustomLink href='/'>
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-4 h-4">
                    <path strokeLinecap="round" strokeLinejoin="round" d="M10.5 19.5 3 12m0 0 7.5-7.5M3 12h18" />
                </svg>
                <span>Home</span>
            </CustomLink>
            <h2>Edit task - {task.name}</h2>
            <FormEditTask currentData={task} />
        </>
    )
}

export default EditTaskPage;