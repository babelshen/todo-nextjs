import TaskInfoItem from "@/components/TaskInfoItem"
import CustomLink from "@/components/UI/CustomLink"
import { dateFormatting } from "@/services/dateFormatting"
import { getTaskById } from "@/services/getTasks"

type Props = {
    params: {
        id: string
    }
}

interface ITask {
    id: string;
    createdAt: Date;
    updatedAt: Date;
    name: string;
    description: string;
    priority: number;
    status: string;
}

export const revalidate = 10;

export async function generateMetadata({params: {id}}: Props) {
    const task = await getTaskById(id);
    return {
        title: `Todo | ${task.name}`,
    }
}

const TaskCard = async ({params: {id}}: Props) => {
    const task: ITask = await getTaskById(id);

    const create = dateFormatting(task.createdAt);
    const update = dateFormatting(task.updatedAt);
    
    return(
        <>
            <CustomLink href='/'>
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-4 h-4">
                    <path strokeLinecap="round" strokeLinejoin="round" d="M10.5 19.5 3 12m0 0 7.5-7.5M3 12h18" />
                </svg>
                <span>Home</span>
            </CustomLink>
            <h2 className='text-center font-semibold text-2xl'>{task.name}</h2>
            <div className='flex justify-between'>
                <div className='flex flex-col md:flex-row gap-2 bg-white items-center p-2 my-2 rounded-lg border-2 border-solid border-slate-300 hover:border-black'>
                    <span className='font-semibold'>Date of create</span>
                    <span>{create.day}.{create.month}.{create.year} at {create.hours}:{create.minutes}</span>
                </div>
                <div className='flex flex-col md:flex-row gap-2 bg-white items-center p-2 my-2 rounded-lg border-2 border-solid border-slate-300 hover:border-black'>
                    <span className='font-semibold'>Date of last update</span>
                    <span>{update.day}.{update.month}.{update.year} at {update.hours}:{update.minutes}</span>
                </div>
            </div>
            <div className='my-7'>
                <TaskInfoItem title='Task details:' value={task.description} />
                <TaskInfoItem title='Task status:' value={task.status} />
                <TaskInfoItem title='Task priority:' value={task.priority} />
            </div>

            <CustomLink href={`${task.id}/edit`}>
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-4 h-4">
                    <path strokeLinecap="round" strokeLinejoin="round" d="m16.862 4.487 1.687-1.688a1.875 1.875 0 1 1 2.652 2.652L10.582 16.07a4.5 4.5 0 0 1-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 0 1 1.13-1.897l8.932-8.931Zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0 1 15.75 21H5.25A2.25 2.25 0 0 1 3 18.75V8.25A2.25 2.25 0 0 1 5.25 6H10" />
                </svg>
                <span>Edit task</span>
            </CustomLink>
        </>
    )
}

export default TaskCard;