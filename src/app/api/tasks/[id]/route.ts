import { NextResponse } from "next/server";

export async function DELETE(req: Request, { params }: { params: { id: string } }) {
    const id = params?.id;

    await fetch(`https://todo-nest-js.onrender.com/api/v1/tasks/${id}`, {
        method: "DELETE",
    })
    
  
    return NextResponse.json({ success: true });
}

export async function PATCH(req: Request, { params }: { params: { id: string }}) {
    const id = params?.id;
    const body = await req.json();

    const task = await fetch(`https://todo-nest-js.onrender.com/api/v1/tasks/${id}`, {
        method: "PATCH",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
    })
    .then(res => res.json());
    
    return NextResponse.json(task);
}