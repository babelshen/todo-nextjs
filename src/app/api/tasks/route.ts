import { NextResponse } from 'next/server';

export async function GET(req: Request) {
    const { searchParams } = new URL(req.url);

    const name = searchParams.get('name') ? searchParams.get('name') : '';
    const priority = searchParams.get('priority') ? searchParams.get('priority') : '';
    const status = searchParams.get('status') ? searchParams.get('status') : '';

    if (status) {
        const tasks = await fetch(`https://todo-nest-js.onrender.com/api/v1/tasks?name=${name}&status=${status}&priority=${priority}`)
            .then(res => res.json())
        
        return NextResponse.json(tasks)
    } else {
        const tasks = await fetch(`https://todo-nest-js.onrender.com/api/v1/tasks?name=${name}&priority=${priority}`)
            .then(res => res.json())
            
        return NextResponse.json(tasks)
    }
}

export async function POST(req: Request) {
    const body = await req.json();

    const task = await fetch(`https://todo-nest-js.onrender.com/api/v1/tasks`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
    })
    .then(res => res.json());
    
    

    return NextResponse.json(task);
}