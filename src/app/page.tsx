import DataManipulation from "@/components/DataManipulation";
import TaskList from "@/components/TaskList";

const MainPage = () => {
  return (
    <>
      <h2 className="text-center font-semibold text-2xl">Your tasks</h2>
      <DataManipulation />
      <TaskList />
    </>
  );
}

export default MainPage;
