'use client';

import { getAllTasksWithParams } from "@/services/getTasks";
import { FormEventHandler, useState } from "react";
import useSWR from "swr";
import CustomSearch from "./UI/CustomSearch";
import CustomSort from "./UI/CustomSort";

const DataManipulation = () => {

    const { mutate } = useSWR("tasks");
    
    const [search, setSearch] = useState<string>('');
    const [priority, setPriority] = useState<string>('');
    const [status, setStatus] = useState<string>('');

    const handleSubmit: FormEventHandler<HTMLFormElement> = async (event) => {
        event.preventDefault();
        const tasks = await getAllTasksWithParams(search, priority, status);
        mutate(tasks);
    }

    const prioritySortList = [
        {
            sortName: 'All',
            sortPath: '',
        },
        {
            sortName: 'Ascending order',
            sortPath: 'ASC',
        },
        {
            sortName: 'Descending order',
            sortPath: 'DESC',
        },
    ];

    const statusSortList = [
        {
            sortName: 'All',
            sortPath: '',
        },
        {
            sortName: 'undone',
            sortPath: 'undone',
        },
        {
            sortName: 'process',
            sortPath: 'process',
        },
        {
            sortName: 'done',
            sortPath: 'done',
        },
    ];
    
    return(
        <>
            <CustomSearch search={search} setSearch={setSearch} handleSubmit={handleSubmit} />
            <div className="flex justify-between w-full">
                <CustomSort setSortParam={setPriority} handleSubmit={handleSubmit} sortList={prioritySortList} title='priority' />
                <CustomSort setSortParam={setStatus} handleSubmit={handleSubmit} sortList={statusSortList} title='status' />
            </div>
        </>
    )
}

export default DataManipulation;