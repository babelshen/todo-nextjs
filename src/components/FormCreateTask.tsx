'use client';

import { createTask } from "@/services/createTask";
import { useForm } from "react-hook-form";
import CustomInput from "./UI/CustomInput";
import { useRouter } from "next/navigation";
import CustomButton from "./UI/CustomButton";

interface IDataCreate {
  name: string;
  description: string;
  priority?: number;
}

const FormCreateTask = () => {

    const { register, handleSubmit, formState: { errors } } = useForm<IDataCreate>({ mode: 'onBlur' });
    
    const onSubmit = async (data: IDataCreate) => {
      if (data.priority) data.priority = Number(data.priority);
      else data.priority = 1;
      const task = await createTask(data);
      console.log(task);
      replace('/');
    }

    const {replace} = useRouter()

    return (
      <form onSubmit={handleSubmit(onSubmit)} className='w-full text-center'>
        <CustomInput
          type="text"
          id="taskname"
          label="Task title"
          placeholder="Type task title"
          error={errors.name}
          {...register('name', {
            required: 'This field must be filled in',
            minLength: {
              value: 3,
              message: 'Task title is too short. Minimum characters: 3',
            },
            maxLength: {
              value: 15,
              message: 'Task title is too long. Maximum characters: 15',
            },
          })}
        />

        <CustomInput
          type="text"
          id="taskdetails"
          label="Task details"
          placeholder="Type task details"
          error={errors.description}
          {...register('description', {
            required: 'This field must be filled in',
            minLength: {
              value: 5,
              message: 'Text in this field is too short. Minimum characters: 5',
            },
            maxLength: {
              value: 50,
              message: 'Text in this field is too long. Maximum characters: 50',
            },
          })}
        />

        <CustomInput
          type="number"
          id="taskpriority"
          label="Task priority"
          placeholder="Task priority"
          error={errors.priority}
          {...register('priority', {
            required: false,
            min: {
              value: 1,
              message: 'The minimum priority is 1',
            },
            max: {
              value: 10,
              message: 'The maximum priority is 10',
            }
          })}
        />

      <CustomButton onClick={handleSubmit(onSubmit)}>
        Create
      </CustomButton>
    </form>
    )
}

export default FormCreateTask;