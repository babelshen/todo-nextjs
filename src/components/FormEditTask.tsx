'use client';

import { useForm } from "react-hook-form";
import CustomInput from "./UI/CustomInput";
import { updateTask } from "@/services/updateTask";
import { ITask } from "@/app/[id]/edit/interface";
import { useState } from "react";
import CustomButton from "./UI/CustomButton";
import { useRouter } from "next/navigation";
import CustomRadioButton from "./UI/CustomRadioButton";

interface IFormEditTaskProps {
    currentData: ITask;
}

const FormEditTask = ({currentData}: IFormEditTaskProps) => {

    const [status, setStatus] = useState(currentData.status);

    const { register, handleSubmit, formState: { errors } } = useForm<ITask>({ mode: 'onBlur', defaultValues:{
        name: currentData.name,
        description: currentData.description,
        priority: currentData.priority,
        status: currentData.status
    }});
    
    const onSubmit = async (data: ITask) => {
        if (data.priority) data.priority = Number(data.priority);
        else data.priority = Number(currentData.priority);
        data.status = status;
        await updateTask(currentData.id, data);
        replace('/');
    }

    const {replace} = useRouter()

    return (
      <form onSubmit={handleSubmit(onSubmit)} className='w-full text-center'>
        <CustomInput
          type="text"
          id="taskname"
          label="Task title"
          placeholder="Type task title"
          error={errors.name}
          {...register('name', {
            required: 'This field must be filled in',
            minLength: {
              value: 3,
              message: 'Task title is too short. Minimum characters: 3',
            },
            maxLength: {
              value: 15,
              message: 'Task title is too long. Maximum characters: 15',
            },
          })}
        />

        <CustomInput
          type="text"
          id="taskdetails"
          label="Task details"
          placeholder="Type task details"
          error={errors.description}
          {...register('description', {
            required: 'This field must be filled in',
            minLength: {
              value: 5,
              message: 'Text in this field is too short. Minimum characters: 5',
            },
            maxLength: {
              value: 50,
              message: 'Text in this field is too long. Maximum characters: 50',
            },
          })}
        />

        <CustomInput
          type="number"
          id="taskpriority"
          label="Task priority"
          placeholder="Task priority"
          error={errors.priority}
          {...register('priority', {
            required: false,
            min: {
              value: 1,
              message: 'The minimum priority is 1',
            },
            max: {
              value: 10,
              message: 'The maximum priority is 10',
            }
          })}
        />

        <CustomRadioButton setParam={setStatus} param={status} title='Choose status:' listParams={['undone', 'process', 'done']} />

        <CustomButton onClick={handleSubmit(onSubmit)}>
            Update
        </CustomButton>
    </form>
    )
}

export default FormEditTask