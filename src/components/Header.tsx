const Header = () => {
    return(
        <header className="bg-black	text-white py-4">
            <div className="mx-6">
                <h1>Automaze | Todo application</h1>
            </div>
        </header>
    )
}

export default Header;