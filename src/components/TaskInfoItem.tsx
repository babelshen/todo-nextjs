const TaskInfoItem = ({ title, value }: { title: string; value: string | number }) => {
    return (
        <div className={`bg-white flex gap-2 items-center p-2 my-2 rounded-lg border-2 border-solid border-slate-300 hover:border-black`}>
            <span className='font-semibold'>{title}</span>
            <span>{value}</span>
        </div>
    )
}

export default TaskInfoItem;