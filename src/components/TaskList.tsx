'use client';

import useSWR from 'swr';
import Link from "next/link";
import { getAllTasks } from '@/services/getTasks';
import Image from 'next/image';
import { deleteTask } from '@/services/deleteTask';
import CustomLink from './UI/CustomLink';

interface ITask {
    id: string;
    createdAt: Date;
    updateAt: Date;
    name: string;
    description: string;
    priority: number;
    status: string;
}

const TaskList = () => {
    const {data:tasks, isLoading} = useSWR('tasks', getAllTasks);

    const { mutate } = useSWR("tasks");

    const handleDelete = async (taskId: string) => {
        try {
          await deleteTask(taskId);
          mutate(tasks); 
        } catch (e) {
          console.log(e);
        }
      };
    return isLoading 
        ? <Image className='mx-auto' src='/loading.svg' width={200} height={200} alt='Loader' priority />
        : (
            <>
                <CustomLink href='/create'>
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-4 h-4">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
                    </svg>
                    <span>Create new task</span>
                </CustomLink>
                <ul>
                {tasks.map((task: ITask) => (
                    <li key={task.id} className={`bg-white grid grid-cols-3 items-center p-2 my-2 rounded-lg border-2 border-solid border-slate-300 hover:border-black`}>
                        <Link href={`${task.id}`} className='inline-flex gap-1 items-center'>
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-4 h-4">
                                <path strokeLinecap="round" strokeLinejoin="round" d="M6 6.878V6a2.25 2.25 0 0 1 2.25-2.25h7.5A2.25 2.25 0 0 1 18 6v.878m-12 0c.235-.083.487-.128.75-.128h10.5c.263 0 .515.045.75.128m-12 0A2.25 2.25 0 0 0 4.5 9v.878m13.5-3A2.25 2.25 0 0 1 19.5 9v.878m0 0a2.246 2.246 0 0 0-.75-.128H5.25c-.263 0-.515.045-.75.128m15 0A2.25 2.25 0 0 1 21 12v6a2.25 2.25 0 0 1-2.25 2.25H5.25A2.25 2.25 0 0 1 3 18v-6c0-.98.626-1.813 1.5-2.122" />
                            </svg>
                            <span>{task.name}</span></Link>
                            <div className='flex flex-col items-center sm:flex-row justify-between'>
                        <span className='flex gap-1'>Status: <p className='font-semibold'>{task.status}</p></span>
                        <span className='flex gap-1'>Priority: <p className='font-semibold'>{task.priority}</p></span>
                        </div>
                        <div className='text-right'>
                            <button className='max-w-6 align-bottom' type='button' onClick={() => handleDelete(task.id)}>
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6 hover:opacity-50 transition-all duration-200 ease-in-out">
                                    <path strokeLinecap="round" strokeLinejoin="round" d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0" />
                                </svg>
                            </button>
                        </div>
                    </li>
                ))}
                </ul>
            </>
        );
}

export default TaskList;