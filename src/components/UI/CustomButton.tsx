import { FormEventHandler, MouseEventHandler } from "react";

interface ICustomButton {
    onClick: FormEventHandler<HTMLFormElement>
    children: React.ReactNode;
}

const CustomButton = ({onClick, children}: ICustomButton) => {
    return(
        <button 
            type='submit'
            onClick={() => onClick}
            className="p-2 bg-black rounded-lg mx-auto text-white hover:bg-neutral-300 hover:text-black border-black border-2 border-solid transition-all	duration-200 ease-in-out"
      >
        {children}
      </button>
    )
}

export default CustomButton;