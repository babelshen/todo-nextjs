'use client';

import React from 'react';
import { FieldError } from 'react-hook-form';

const CustomInput = React.forwardRef(
    ({ type, id, label, placeholder, error, ...props }: { type: string; id: string; label: string; placeholder: string; error?: FieldError }, ref: React.Ref<HTMLInputElement>) => (
      <label htmlFor={id} className="w-full flex flex-col gap-2 font-semibold text-base my-4 text-left">
        {label}
        <input 
            type={type} 
            placeholder={placeholder} 
            id={id} 
            ref={ref} 
            className={`w-full p-2 border-2 border-solid rounded-lg focus:border-black hover:border-black focus:outline-none transition-all duration-200 ease-in-out ${error ? 'border-red-500' : 'border-slate-300'}`} 
            {...props} 
        />
        {error ? <p className="text-red-500 text-left">{error.message}</p> : false}
      </label>
    ),
  );

  CustomInput.displayName = 'CustomInput';

  export default CustomInput;
  