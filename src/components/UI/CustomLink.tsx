import Link from "next/link"

const CustomLink = ({href, children}: {href:string; children: React.ReactNode;}) => {
    return(
        <Link 
            href={href}
            className='inline-flex gap-1 items-center mb-7 mt-2'
        >
            {children}
        </Link>
    )
}

export default CustomLink;