interface ICustomRadioButton {
    setParam: (param: string) => void;
    param: string;
    title: string;
    listParams: string[];
}

const CustomRadioButton = ({setParam, param, title, listParams}: ICustomRadioButton) => {
    return (
        <div className='flex items-center justify-between mb-7'>
            <div className='font-semibold'>{title}</div>
            {listParams.map(item => (
                <div 
                    key={item}
                    className={`flex flex-col items-center p-2 border-2 border-solid rounded-lg cursor-pointer w-1/5 ${item === 'undone' ? 'bg-neutral-200 border-black' : 'bg-white border-slate-300'}`}
                    onClick={() => {setParam(item)}}
                >
                    {param === item
                        ? <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round" d="m4.5 12.75 6 6 9-13.5" />
                        </svg>
                        : <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                            <path strokeLinecap="round" strokeLinejoin="round" d="M6 18 18 6M6 6l12 12" />
                        </svg>
                    }
                    <span>{item}</span>
                </div>
            ))}
        </div>
    )
}

export default CustomRadioButton;