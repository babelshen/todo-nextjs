'use client';

import { FormEventHandler } from "react";
import CustomButton from "./CustomButton";

interface ICustomSearch {
  search: string;
  setSearch: (param: string) => void;
  handleSubmit: FormEventHandler<HTMLFormElement>
}

const CustomSearch = ({search, setSearch, handleSubmit}: ICustomSearch) => {

  return (
    <form onSubmit={handleSubmit} className="w-full	flex gap-1.5 my-4">
      <input
        type="search"
        placeholder="Search task"
        value={search}
        onChange={(event) => setSearch(event.target.value)}
        className="w-full p-2 border-2 border-solid border-slate-300 rounded-lg focus:border-black hover:border-black focus:outline-none transition-all duration-200 ease-in-out"
      />
      <CustomButton onClick={handleSubmit}>Search</CustomButton>
    </form>
  );
};

export default CustomSearch;