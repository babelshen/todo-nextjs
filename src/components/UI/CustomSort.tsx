'use client';

import { FormEventHandler, useState } from "react";

interface ICustomSort {
    setSortParam: (param: string) => void;
    handleSubmit: FormEventHandler<HTMLFormElement>;
    sortList: {
        sortName: string,
        sortPath: string,
    }[];
    title: string;
}

const CustomSort = ({setSortParam, handleSubmit, sortList, title}: ICustomSort) => {

    const [openMenu, setOpenMenu] = useState<boolean>(false);
    const [option, setOption] = useState<string>('All');
    

    return(
        <form onSubmit={handleSubmit} className='w-2/6 flex flex-col gap-2.5 mb-7'>
            <span className="font-semibold">Sort by {title}</span>
            <div className={`w-full relative flex justify-between bg-white p-2 border-2 border-solid border-slate-300 rounded-lg hover:border-black ${openMenu ? 'border-black rounded-b-none' : 'border-slate-300'}`}>
                <span className="font-semibold">{option}</span>
                <button type='button' onClick={() => setOpenMenu(!openMenu)}>
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className={`w-4 h-4 hover:opacity-60 ${openMenu ? 'rotate-0 transition-all duration-200 ease-in-out' : 'rotate-180 transition-all duration-200 ease-in-out'}`}>
                        <path strokeLinecap="round" strokeLinejoin="round" d="m19.5 8.25-7.5 7.5-7.5-7.5" />
                    </svg>
                </button>
                {openMenu 
                    ? <div className={`absolute w-full flex flex-col top-full inset-x-0	bg-white border-2 border-t-0 border-solid transition-all duration-200 ease-in-out ${openMenu ? 'border-black ' : 'border-slate-300'}`}>
                        { sortList.map(item => (
                            <button key={item.sortName} type="submit" onClick={()=>{
                                setOption(item.sortName)
                                setOpenMenu(false)
                                setSortParam(item.sortPath)
                            }} className={`w-full p-2 text-left ${option === item.sortName ? 'bg-neutral-300 font-semibold' : false}`}>{item.sortName}</button>
                        ))}
                        </div>
                    : false
                }
            </div>    
            <button className="p-2 bg-black rounded-lg text-white hover:bg-neutral-300 hover:text-black border-black border-2 border-solid transition-all	duration-200 ease-in-out" type="submit">Submit</button>
        </form>
    )
}

export default CustomSort;