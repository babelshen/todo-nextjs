interface IPostBody {
    name: string;
    description: string;
    priority?: number;
}

  export const createTask = async (body: IPostBody) => {
    const response = await fetch(`/api/tasks`, {
        method: 'POST',
        body: JSON.stringify(body),
    })
    return response.json();
  }