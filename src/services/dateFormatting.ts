export const dateFormatting = (currentFormat: Date) => {
    const createDate = new Date(currentFormat);
    const day = createDate.getDate();
    const month = createDate.getMonth() + 1;
    const year = createDate.getFullYear();
    const hours = createDate.getHours();
    const minutes = createDate.getMinutes();

    return {day, month, year, hours, minutes};
}