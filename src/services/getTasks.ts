export const getAllTasks = async () => {
    const response = await fetch(`/api/tasks`, {
        next: {
          revalidate: 30,
        }
      });
    if (!response.ok) throw new Error("Unable to fetch tasks.");
    return response.json();
}

export const getAllTasksWithParams = async (search: string, priority:string, status:string) => {
    const response = await fetch(`/api/tasks?name=${search}&priority=${priority}&status=${status}`, {
        next: {
          revalidate: 30,
        }
      });
    if (!response.ok) throw new Error("Unable to fetch tasks.");
    return response.json();
}

export const getTaskById = async (id:string) => {
    const response = await fetch(`https://todo-nest-js.onrender.com/api/v1/tasks/${id}`);
    return response.json();
}