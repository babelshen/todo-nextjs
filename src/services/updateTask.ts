interface IPatchBody {
    name?: string;
    description?: string;
    priority?: number;
    status?: string
}

  export const updateTask = async (id: string, body: IPatchBody) => {
    const response = await fetch(`/api/tasks/${id}`, {
        method: 'PATCH',
        body: JSON.stringify(body),
    })
    return response.json();
  }